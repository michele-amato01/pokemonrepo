//
//  PokemonAPITest.swift
//  PokemonTests
//
//  Created by Michele Amato on 22/11/2020.
//

import XCTest
@testable import PokemonProject

class PokemonAPITest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    
    func testPokemonList() throws {

        let  pokemonListJson: String =
        """
                    {
                        "count": 1050,
                        "next": "https://pokeapi.co/api/v2/pokemon?offset=3&limit=3",
                        "previous": null,
                        "results": [{
                            "name": "bulbasaur",
                            "url": "https://pokeapi.co/api/v2/pokemon/1/"
                        }, {
                            "name": "ivysaur",
                            "url": "https://pokeapi.co/api/v2/pokemon/2/"
                        }, {
                            "name": "venusaur",
                            "url": "https://pokeapi.co/api/v2/pokemon/3/"
                        }]
                    }
        """

        let pokemonListData = pokemonListJson.data(using: .utf8)!
        let response = try JSONDecoder().decode(PokemonListAPI.Output.self, from: pokemonListData)

        XCTAssertNil(response.previous, "Previous is nil")
        XCTAssertNotNil(response.count)
        let result = response.results
        XCTAssertEqual(result?[0].name, "bulbasaur")
        XCTAssertNotNil(result)
    }
    
    func testPokemonAPI() throws {

        guard

            let path = Bundle(for: type(of: self)).path(forResource: "pokemon", ofType: "json")
        else {
            fatalError("Can't find pokemon.json file")
        }

        let data = try Data(contentsOf: URL(fileURLWithPath: path))
        let response = try JSONDecoder().decode(Pokemon.self, from: data)
        
        print("Response \(response)")
        XCTAssertEqual(response.name, "ditto")
        XCTAssertEqual(response.base_experience, 101)
        XCTAssertEqual(response.height, 3)
        XCTAssertEqual(response.order, 203)
        XCTAssertTrue(((response.types?.isEmpty) != nil))
    }
    
    

}
