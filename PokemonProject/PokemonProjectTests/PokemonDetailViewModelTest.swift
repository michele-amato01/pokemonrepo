//
//  PokemonDetailViewModelTest.swift
//  PokemonTests
//
//  Created by Michele Amato on 24/11/2020.
//

import XCTest
@testable import PokemonProject

class PokemonDetailViewModelTest: XCTestCase {
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testPokemonDetailViewModel_successCase() throws {
        
        guard
            let path = Bundle(for: type(of: self)).path(forResource: "pokemon", ofType: "json")
        else {
            fatalError("Can't find pokemon.json file")
        }
        
        let data = try Data(contentsOf: URL(fileURLWithPath: path))
        let pokemon = try JSONDecoder().decode(Pokemon.self, from: data)
        
        let pokemonDetail = PokemonDetail.init(from: pokemon)
        
        let sut = PokemonDetailViewModel.init(pokemon: pokemonDetail)
        let expectation = XCTestExpectation(description: "Expect to load pokemon detail")
        
        sut.pokemonDetailViewModel.bind { value  in
            print("Value \(value)")
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1.0)
        let infoModel = sut.pokemonDetailViewModel.value[2].items[0] as! PokemonInfoViewModel
        XCTAssertEqual(infoModel.itemValue[0].value, "101", "pokemon detail") // base exp

        let typeModel = sut.pokemonDetailViewModel.value[3].items[0] as! PokemonTypeViewModel
        XCTAssertEqual(typeModel.pokemonType[0], PokemonTypeModel.normal, "pokemon detail") // type grass
    }
}
