//
//  PokemonDetailTest.swift
//  PokemonTests
//
//  Created by Michele Amato on 22/11/2020.
//

import XCTest
@testable import PokemonProject

class PokemonDetailTest: XCTestCase {
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testPokemonDetail() throws {
        
        guard
            
            let path = Bundle(for: type(of: self)).path(forResource: "pokemon", ofType: "json")
        else {
            fatalError("Can't find pokemon.json file")
        }
        
        let data = try Data(contentsOf: URL(fileURLWithPath: path))
        let pokemon = try JSONDecoder().decode(Pokemon.self, from: data)
        
        let pokemonDetail = PokemonDetail.init(from: pokemon)
        XCTAssertNotNil(pokemonDetail)
        XCTAssertEqual(pokemonDetail.name, "ditto")
        XCTAssertEqual(pokemonDetail.defaultImage, "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/132.png")
    }
}
    
    
