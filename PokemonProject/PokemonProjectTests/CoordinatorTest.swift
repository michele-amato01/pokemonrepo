//
//  CoordinatorTest.swift
//  PokemonTests
//
//  Created by Michele Amato on 22/11/2020.
//

import XCTest
@testable import PokemonProject

class CoordinatorTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testAddCoordinator() {
        let fakeCoordinator1 = FakeCoordinator.init()
        let child1Coordinator = FakeChildCoordinator.init()
        fakeCoordinator1.add(childCoordinator: child1Coordinator)
        XCTAssert(fakeCoordinator1.childCoordinators.count == 1)
        XCTAssert(fakeCoordinator1.presentedCoordinator === child1Coordinator)
    }
    
    func testAddSelfCoordinator() {
        let fakeCoordinator1 = FakeCoordinator.init()
        fakeCoordinator1.add(childCoordinator: fakeCoordinator1)
        XCTAssert(fakeCoordinator1.childCoordinators.count == 0)
        XCTAssertNil(fakeCoordinator1.presentedCoordinator)
    }
    
    func testRemoveCoordinator() {
        let fakeCoordinator1 = FakeCoordinator.init()
        let child1Coordinator = FakeChildCoordinator.init()
        fakeCoordinator1.add(childCoordinator: child1Coordinator)
        XCTAssert(fakeCoordinator1.childCoordinators.count == 1)
        XCTAssert(fakeCoordinator1.presentedCoordinator === child1Coordinator)
        fakeCoordinator1.remove(childCoordinator: child1Coordinator)
        XCTAssert(fakeCoordinator1.childCoordinators.count == 0)
    }

    func testSearchCoordinator() {
        let fakeCoordinator1 = FakeCoordinator.init()
        let child1Coordinator = FakeChildCoordinator.init()
        let child2Coordinator = FakeChild2Coordinator.init()
        fakeCoordinator1.add(childCoordinator: child1Coordinator)
        XCTAssert(fakeCoordinator1.search(type: FakeChildCoordinator.self) === child1Coordinator)
        fakeCoordinator1.add(childCoordinator: fakeCoordinator1)
        XCTAssert((fakeCoordinator1.search(type: FakeCoordinator.self) === fakeCoordinator1))
        
        
        fakeCoordinator1.add(childCoordinator: child2Coordinator)
        XCTAssert((fakeCoordinator1.search(type: FakeChild2Coordinator.self) === child2Coordinator))
        
        fakeCoordinator1.remove(childCoordinator: child1Coordinator)
        XCTAssertNil(fakeCoordinator1.search(type: FakeChildCoordinator.self))
    }
}

final class FakeCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    
    var rootViewController: UIViewController = UIViewController()
    
    var service: Service = Service.createService()
    
    func start(with option: DeepLinkOption?) {
        
    }
}

final class FakeChildCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    
    var rootViewController: UIViewController = UIViewController()
    
    var service: Service = Service.createService()

    func start(with option: DeepLinkOption?) {
        
    }
}

final class FakeChild2Coordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    
    var rootViewController: UIViewController = UIViewController()
    
    var service: Service = Service.createService()

    func start(with option: DeepLinkOption?) {
        
    }
}


