//
//  PokemonViewModelTest.swift
//  PokemonTests
//
//  Created by Michele Amato on 24/11/2020.
//

import XCTest
@testable import PokemonProject

class PokemonViewModelTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testPokemonViewModel_successCase() throws {
        
        let serviceTest: Service = MocksProvider.serviceTest(mocks: MocksProvider.pokemonManagerSuccessMocks(),
                                                             requestsBlock: { requestPath in
                                                                print("Request called \(requestPath)")

        })
        
        let sut = PokemonViewModel.init(service: serviceTest)
        let expectation = XCTestExpectation(description: "Expect to load pokemon list")
        
        sut.pokemonList.bind { value  in
            if !value.isEmpty {
                expectation.fulfill()
            }
        }
        
        let imageURLString = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/1.png"
        
        let pokemon: [[ItemViewModel]] = [[PokemonListViewModel.init(title: "bulbasaur", imageURLString: imageURLString)], [LoadMoreViewModel.init()]]
        
        wait(for: [expectation], timeout: 30.0)
    
        XCTAssertEqual(sut.pokemonList.value.count, pokemon.count, "pokemon list")

    }
    
    
    func testPokemonViewModel_errorCase() throws {
        
        let serviceTest: Service = MocksProvider.serviceTest(mocks: MocksProvider.pokemonManagerFailureMocks(),
                                                             requestsBlock: { requestPath in
                                                                print("Request called \(requestPath)")

        })
        
        let sut = PokemonViewModel.init(service: serviceTest)
        let expectation = XCTestExpectation(description: "Expect to receive error from pokemon list")
        
        sut.error.bind { error in
            print("Error \(sut.error.value)")
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 5.0)
    
        XCTAssert(!sut.error.value.isEmpty)
    }
    

}
