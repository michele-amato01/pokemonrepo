//
//  ViewModelTest.swift
//  PokemonTests
//
//  Created by Michele Amato on 22/11/2020.
//

import XCTest
@testable import PokemonProject

class ViewModelTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    
    func testPokemonListViewModel() {
        let imageURLString = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/1.png"
        let viewModel = PokemonListViewModel.init(title: "Pikachu", imageURLString: imageURLString)
        XCTAssertEqual(viewModel.title, "Pikachu")
        XCTAssertEqual(viewModel.imageURLString, "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/1.png")

    }
    
    func testPokemonImageViewModel() {
        let defaultImage = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/1.png"
        let viewModel = PokemonImageViewModel.init(defaultImage: defaultImage)
        XCTAssertEqual(viewModel.defaultImage, "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/1.png")
    }
    
    func testPokemonImagesViewModel() {
        let back = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/2.png"
        let front = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/2.png"

        let viewModel = PokemonImagesViewModel.init(frontImage: front, backImage: back)
        XCTAssertEqual(viewModel.frontImage, "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/2.png")
        XCTAssertEqual(viewModel.backImage, "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/2.png")

    }
    
    func testPokemonInfoViewModel() {
        let itemValue1 = ItemValue.init(item: "first", value: "1")
        let itemValue2 = ItemValue.init(item: "second", value: "2")

        let viewModel = PokemonInfoViewModel.init(itemValue: [itemValue1, itemValue2])
        XCTAssertEqual(viewModel.itemValue[0].item, "first")
        XCTAssertEqual(viewModel.itemValue[0].value, "1")
        XCTAssertEqual(viewModel.itemValue[1].item, "second")
        XCTAssertEqual(viewModel.itemValue[1].value, "2")
        
        let viewModel1 = PokemonInfoViewModel.init()
        XCTAssertTrue(viewModel1.itemValue.isEmpty)
    }
    
    func testPokemonTypeModel() {
        let pokemonType1 = PokemonTypeModel.fire
        let pokemonType2 = PokemonTypeModel.normal
        let viewModel = PokemonTypeViewModel.init(pokemonType: [pokemonType1, pokemonType2])
        XCTAssertEqual(viewModel.pokemonType.count, 2)
        XCTAssertEqual(viewModel.pokemonType[0], pokemonType1)

    }
    
    func testKeyValueViewModel() {
        let viewModel = KeyValueViewModel.init(item: "An item")
        XCTAssertEqual(viewModel.item, "An item")
        XCTAssertEqual(viewModel.value, "")

        let viewModel1 = KeyValueViewModel.init(item: "An item", value: "A value")
        XCTAssertEqual(viewModel1.item, "An item")
        XCTAssertEqual(viewModel1.value, "A value")
    }

}
