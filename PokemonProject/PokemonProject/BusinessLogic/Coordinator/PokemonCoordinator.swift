//
//  PokemonCoordinator.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import Foundation
import UIKit

final class PokemonCoordinator: Coordinator {

    //MARK: - Properties
    var childCoordinators: [Coordinator]
    var service: Service
    var rootViewController: UIViewController = UIViewController()

    //MARK: - Init
    init(service: Service) {
        self.service = service
        self.childCoordinators = []
        let pokemonViewModel = PokemonViewModel.init(service: service)
        let pokemonListViewController = PokemonListViewController(viewModel: pokemonViewModel)
        pokemonListViewController.title = "POKEMON"
        pokemonListViewController.delegate = self
        self.rootViewController = pokemonListViewController
    }
    
    //MARK: - Methods
    func start(with option: DeepLinkOption?) {
        guard let _ = option else {
            return
        }
    }
}

//MARK: - PokemonListViewControllerDelegate
extension PokemonCoordinator: PokemonListViewControllerDelegate {
    func openPokemonDetail(_ pokemonListViewController: PokemonListViewController, for pokemon: PokemonDetail) {
        let pokemonViewModel = PokemonDetailViewModel.init(pokemon: pokemon)
        let pokemonDetailViewController = PokemonDetailViewController(viewModel: pokemonViewModel)
        pokemonDetailViewController.title = pokemon.name.uppercased()
        pokemonListViewController.navigationController?.pushViewController(pokemonDetailViewController, animated: true)
    }
}

