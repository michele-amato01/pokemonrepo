//
//  Pokemon.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import Foundation

//Ability
public class PokemonAbility: CustomCodable  {
    var isHidden: Bool?
    var slot: Int?
    var ability: Item?
    
}

//Type
final class PokemonType: CustomCodable  {
    var slot: Int?
    var type: Item?
}

//Held Item
final class PokemonHeldItem: CustomCodable  {
    var item: [Item]?
    var versionDetails: [PokemonHeldItemVersion]?
}

//Held Item Version
final class PokemonHeldItemVersion: CustomCodable  {
    var version: [Item]?
    var rarity: Int?
}

//Move
final class PokemonMove: CustomCodable  {
    var move: Item?
    var versionGroupDetails: [PokemonMoveVersion]?
}

//Move Version
final class PokemonMoveVersion: CustomCodable  {
    var moveLearnMethod: [Item]?
    var versionGroup: [Item]?
    var levelLearnedAt: Int?
}

//Stat
final class PokemonStat: Decodable  {
    var stat: Item?
    var effort: Int?
    var base_stat: Int?
}


//Sprites
final class PokemonSprites: Decodable  {
    var frontDefault: String?
//    var frontShiny: String?
//    var frontFemale: String?
//    var frontShinyFemale: String?
    var backDefault: String?
//    var backShiny: String?
//    var backFemale: String?
//    var backShinyFemale: String?
    var other: OtherSprites?
    
    enum CodingKeys: String, CodingKey {
        case frontDefault = "front_default"
        case backDefault = "back_default"
        case other = "other"
    }
}

final class OtherSprites: Decodable  {
    var officialArtwork: ArtWork?
    
    enum CodingKeys: String, CodingKey {
        case officialArtwork = "official-artwork"
    }
}

final class ArtWork: Decodable  {
    let frontDefault: String?
    
    enum CodingKeys: String, CodingKey {
        case frontDefault = "front_default"
    }
}


//Location Area Encounter
final class LocationAreaEncounter: CustomCodable  {
    var locationArea: [Item]?
    var versionDetails: [VersionEncounterDetail]?
}


//Pokemon Colors
final class PokemonColor: CustomCodable  {
    var id: Int?
    var name: String?
    var names: [Name]?
    var pokemonSpecies: [Item]
}

//Forms
final class PokemonForm: CustomCodable  {
    var id: Int?
    var name: String?
    var order: Int?
    var formOrder: Int?
    var isDefault: Bool?
    var isBattleOnly: Bool?
    var isMega: Bool?
    var formName: String?
    var pokemon: [Item]?
    var sprites: PokemonFormSprites?
    var versionGroup: [Item]?
    var names: [Name]?
    var formNames: [Name]?
}


// Form Sprites
final class PokemonFormSprites: CustomCodable  {
    var frontDefault: String?
    var frontShiny: String?
    var backDefault: String?
    var backShiny: String?
}

//Habitats
final class PokemonHabitat: CustomCodable  {
    var id: Int?
    var name: String?
    var names: [Name]?
    var pokemonSpecies: [Item]?
}

//Shapes
final class PokemonShape: CustomCodable  {
    var id: Int?
    var name: String?
    var awesomeNames: [AwesomeName]?
    var names: [Name]?
    var pokemonSpecies: [Item]?
}

//AwesomeName
final class AwesomeName: CustomCodable  {
    var awesomeName: String?
    var language: [Item]?
}

//Species
final class PokemonSpecies: CustomCodable  {
    var id: Int?
    var name: String?
    var order: Int?
    var genderRate: Int?
    var captureRate: Int?
    var baseHappiness: Int?
    var isBaby: Bool?
    var hatchCounter: Int?
    var hasGenderDifferences: Bool?
    var formsSwitchable: Bool?
    var growthRate: [Item]?
    var pokedexNumbers: [PokemonSpeciesDexEntry]?
    var eggGroups: [Item]?
    var color: [Item]?
    var shape: [Item]?
    var evolvesFromSpecies: [Item]?
    var evolutionChain:[Item]
    var habitat: [Item]?
    var generation: [Item]?
    var names: [Name]?
    var palParkEncounters: [PalParkEncounterArea]?
    var flavorTextEntries: [FlavorText]?
    var formDescriptions: [Description]?
    var genera: [Genus]?
    var varieties: [PokemonSpeciesVariety]?
}

//Genus
final class Genus: CustomCodable  {
    var genus: String?
    var language: [Item]?
}


//pecies Dex Entry
final class PokemonSpeciesDexEntry: CustomCodable  {
    var entryNumber: Int?
    var name: [Item]?
}


//PalPark Encounter Area
final class PalParkEncounterArea: CustomCodable  {
    var baseScore: Int?
    var rate: Int?
    var area: [Item]?
}


/// Pokemon Species Variety
final class PokemonSpeciesVariety: CustomCodable  {
    var isDefault: Bool?
    var pokemon: [Item]?
}



//Stats
final class Stat: CustomCodable  {
    var id: Int?
    var name: String?
    var gameIndex: Int?
    var isBattleOnly: Bool?
    var affectingMoves: MoveStatAffectSets?
    var affectingNatures: NatureStatAffectSets?
    var characteristics: [Item]?
    var moveDamageClass: [Item]?
    var names: [Name]?
    
}


//Move Stat Affect Sets
final class MoveStatAffectSets: CustomCodable  {
    var increase: [MoveStatAffect]?
    var decrease: [MoveStatAffect]?
}


//Move Stat Affect
final class MoveStatAffect: CustomCodable  {
    var change: Int?
    var move: [Item]?
}


//Nature Affect Set
final class NatureStatAffectSets: CustomCodable  {
    var increase: [Item]?
    var decrease: [Item]?
}



//Types
final class Type: CustomCodable  {
    var id: Int?
    var name: String?
    var damageRelations: TypeRelations?
    var gameIndices: [GenerationGameIndex]?
    var generation: [Item]?
    var moveDamageClass: [Item]?
    var names: [Name]?
    var pokemon: [TypePokemon]?
    var moves: [Item]?
}


//Pokemon Type
final class TypePokemon: CustomCodable  {
    var slot: Int?
    var pokemon: [Item]?
}

//Relations
final class TypeRelations: CustomCodable  {
    var noDamageTo: [Item]?
    var halfDamageTo: [Item]?
    var doubleDamageTo: [Item]?
    var noDamageFrom: [Item]?
    var halfDamageFrom: [Item]?
    var doubleDamageFrom: [Item]?
}


//Languages
final class Language: CustomCodable  {
    var id: Int?
    var name: String?
    var official: Bool?
    var iso639: String?
    var iso3166: String?
    var names: [Name]?
}


//Description
final class Description: CustomCodable  {
    var description: String?
    var language: [Item]?
}

//Effect
final class Effect: CustomCodable  {
    var effect: String?
    var language: [Item]?
}


//Encounter
final class Encounter: CustomCodable  {
    var minLevel: Int?
    var maxLevel: Int?
    var conditionValues: [Item]?
    var chance: Int?
    var method: [Item]?
}


//Flavor Text
final class FlavorText: CustomCodable  {
    var flavorText: String?
    var language: Name?
}


// Generation Game Index
final class GenerationGameIndex: CustomCodable  {
    var gameIndex: Int?
    var generation: [Item]?
}


//Machine Version Detail
final class MachineVersionDetail: CustomCodable  {
    var machine: [Item]?
    var versionGroup: [Item]?
}


// Name
final class Name: CustomCodable  {
    var name: String?
    var language: [Item]?
}


//Verbose Effect
final class VerboseEffect: CustomCodable  {
    var effect: String?
    var shortEffect: String?
    var language: [Item]?
    
}


//Version Encounter Detail
final class VersionEncounterDetail: CustomCodable  {
    var version: [Item]?
    var maxChance: Int?
    var encounterDetails: [Encounter]?
}


//Version Game Index
final class VersionGameIndex: CustomCodable  {
    var gameIndex: Int?
    var version: [Item]?
}


//Version Group Flavor Text
final class VersionGroupFlavorText: CustomCodable  {
    var text: String?
    var language: [Item]?
    var versionGroup: [Item]?
}
