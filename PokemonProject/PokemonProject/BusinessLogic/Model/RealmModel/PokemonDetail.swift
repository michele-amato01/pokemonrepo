//
//  PokemonDetail.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import Foundation
import RealmSwift

final class PokemonDetail: Object {

    @objc dynamic var name: String = ""
    @objc dynamic var defaultImage: String = ""
    @objc dynamic var frontImage: String = ""
    @objc dynamic var backImage: String = ""

    dynamic var informations = List<ItemValue>()
    dynamic var abilities = List<String>()
    dynamic var stats = List<ItemValue>()
    dynamic var types = List<String>()

    override class func primaryKey() -> String? {
           return "name"
       }
    
    convenience init(from pokemon: Pokemon) {
        self.init()
        
        self.name = pokemon.name ?? ""
        
        var defaultImage = ""
        var frontImage = ""
        var backImage = ""
             
        if let sprites = pokemon.sprites {
            
            if let others = sprites.other,
            let artwork = others.officialArtwork,
            let frontDefault = artwork.frontDefault {
                defaultImage = frontDefault
            }
            
            if let front = sprites.frontDefault {
                frontImage = front
            }
            
            if let back = sprites.backDefault {
                backImage = back
            }
        }
        
        self.defaultImage = defaultImage
        self.frontImage = frontImage
        self.backImage = backImage


        let types = List<String>()
        
        if let pokemonTypes = pokemon.types, pokemonTypes.count > 0 {
            pokemonTypes.forEach{ element in
                if let type = element.type, let name = type.name {
                    types.append(name)
                }
            }
        }
        
        self.types = types
        
        let informations =  List<ItemValue>()
        
        var baseExperience = "-"
        if let baseExp = pokemon.base_experience {
            baseExperience = "\(baseExp)"
        }
        informations.append(ItemValue.init(item: "BASE EXP.", value: "\(baseExperience)"))
        
        var weight = "-"
        if let _weight = pokemon.weight {
            weight = "\(_weight)"
        }
        informations.append(ItemValue.init(item: "WEIGHT", value: "\(weight)"))
        
        var height = "-"
        if let _height = pokemon.height {
            height = "\(_height)"
        }
        
        informations.append(ItemValue.init(item: "HEIGHT", value: "\(height)"))
        
        self.informations = informations
        
        let abilities = List<String>()
        if let _abilities = pokemon.abilities {
            _abilities.forEach { singleAbility in
                if let ability = singleAbility.ability, let name = ability.name {
                    abilities.append(name.capitalized)
                }
            }
        }
        
        self.abilities = abilities
        
        let stats = List<ItemValue>()
        
        if let _stats = pokemon.stats {
            _stats.forEach { pokemonStats in
                if let baseStat = pokemonStats.base_stat, let stat = pokemonStats.stat, let name = stat.name {
                    stats.append(ItemValue.init(item: name.capitalized, value: "\(baseStat)"))
                }
            }
        }
        
        self.stats = stats
    }

}

final class ItemValue: Object {
    @objc dynamic var item: String = ""
    @objc dynamic var value: String = ""
    
    convenience init(item: String, value: String) {
        self.init()
        self.item = item
        self.value = value
    }
    
    convenience init(item: String) {
        self.init()
        self.item = item
        self.value = ""
    }
}
