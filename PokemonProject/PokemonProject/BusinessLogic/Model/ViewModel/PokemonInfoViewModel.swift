//
//  PokemonInfoViewModel.swift
//  Pokemon
//
//  Created by Michele Amato on 17/11/2020.
//

import Foundation

final class PokemonInfoViewModel: ItemViewModel {
    
    var itemIdentifier: ListIdentifier = PokemonInfoCell.itemIdentifier
    
    var itemValue: [ItemValue]
    
    init(itemValue: [ItemValue]) {
        self.itemValue = itemValue
    }
    
    init() {
        self.itemValue = []
    }
}

extension PokemonInfoViewModel: Equatable {
    static func == (lhs: PokemonInfoViewModel, rhs: PokemonInfoViewModel) -> Bool {
        return lhs.itemValue == rhs.itemValue
    }
}

