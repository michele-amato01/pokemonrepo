//
//  PokemonImageViewModel.swift
//  Pokemon
//
//  Created by Michele Amato on 17/11/2020.
//

import Foundation

final class PokemonImageViewModel: ItemViewModel {
    
    var itemIdentifier: ListIdentifier = PokemonImageCell.itemIdentifier

    var defaultImage: String

    init(defaultImage: String) {
        self.defaultImage = defaultImage
    }
}

extension PokemonImageViewModel: Equatable {
    static func == (lhs: PokemonImageViewModel, rhs: PokemonImageViewModel) -> Bool {
        return lhs.defaultImage == rhs.defaultImage
    }
}
