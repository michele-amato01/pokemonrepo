//
//  PokemonListViewModel.swift
//  Pokemon
//
//  Created by Michele Amato on 18/11/2020.
//

import Foundation


final class PokemonListViewModel: ItemViewModel {
    
    var itemIdentifier: ListIdentifier = PokemonCell.itemIdentifier
    
    var title: String
    var imageURLString: String

    init(title: String, imageURLString: String) {
        self.title = title
        self.imageURLString = imageURLString
    }
}

extension PokemonListViewModel: Equatable {
    static func == (lhs: PokemonListViewModel, rhs: PokemonListViewModel) -> Bool {
        return lhs.title == rhs.title
    }
}


