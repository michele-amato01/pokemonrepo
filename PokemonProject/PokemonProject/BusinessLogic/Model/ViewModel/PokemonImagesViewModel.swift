//
//  PokemonImagesViewModel.swift
//  Pokemon
//
//  Created by Michele Amato on 29/11/2020.
//

final class PokemonImagesViewModel: ItemViewModel {
    
    var itemIdentifier: ListIdentifier = PokemonImagesCell.itemIdentifier

    var frontImage: String
    var backImage: String

    init(frontImage: String, backImage: String) {
        self.frontImage = frontImage
        self.backImage = backImage
    }
}

extension PokemonImagesViewModel: Equatable {
    static func == (lhs: PokemonImagesViewModel, rhs: PokemonImagesViewModel) -> Bool {
        return lhs.frontImage == rhs.frontImage && lhs.backImage == rhs.backImage
    }
}
