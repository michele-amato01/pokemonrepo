//
//  PokemonTypeViewModel.swift
//  Pokemon
//
//  Created by Michele Amato on 17/11/2020.
//

import Foundation

final class PokemonTypeViewModel: ItemViewModel {
    
    var itemIdentifier: ListIdentifier = PokemonTypeCell.itemIdentifier
    
    var pokemonType: [PokemonTypeModel]
    
    init(pokemonType: [PokemonTypeModel]) {
        self.pokemonType = pokemonType
    }
}

extension PokemonTypeViewModel: Equatable {
    static func == (lhs: PokemonTypeViewModel, rhs: PokemonTypeViewModel) -> Bool {
        return lhs.pokemonType == rhs.pokemonType
    }
}

