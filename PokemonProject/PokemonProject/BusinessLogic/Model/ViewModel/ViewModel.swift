//
//  ViewModel.swift
//  Pokemon
//
//  Created by Michele Amato on 17/11/2020.
//

import Foundation

public protocol ViewModel {}

public protocol ItemViewModel: ViewModel   {
    var itemIdentifier: ListIdentifier { get }
}

public protocol ListIdentifier {
    var name : String { get }
}

public protocol ViewModelBindable {
    func bind(to viewModel: ViewModel)
}

fileprivate struct ViewReuseId: ListIdentifier {
    var name: String
}

extension ViewModelBindable  {
    
   public static var itemIdentifier: ListIdentifier {
        return ViewReuseId(name: String(describing: self))
    }
}

