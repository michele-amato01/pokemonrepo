//
//  KeyValueViewModel.swift
//  Pokemon
//
//  Created by Michele Amato on 17/11/2020.
//

import Foundation

final class KeyValueViewModel: ItemViewModel {
    
    var itemIdentifier: ListIdentifier = KeyValueCell.itemIdentifier
    
    var item: String
    var value: String

    init(item: String, value: String) {
        self.item = item
        self.value = value
    }
    
    init(item: String) {
        self.item = item
        self.value = ""
    }
}


extension KeyValueViewModel: Equatable {
    static func == (lhs: KeyValueViewModel, rhs: KeyValueViewModel) -> Bool {
        return lhs.item == rhs.item && lhs.value == rhs.value
    }
}


