//
//  Observable.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import Foundation

final class Observable<T> {

    var value: T {
        didSet {
            listener?(value)
        }
    }

    private var listener: ((T) -> Void)?

    init(_ value: T) {
        self.value = value
    }

    func bind(_ closure: @escaping (T) -> Void) {
        closure(value)
        listener = closure
    }
}

//public final class Observable<Value> {
//    
//    struct Observer<Value> {
//        weak var observer: AnyObject?
//        let block: (Value) -> Void
//    }
//    
//    private var observers = [Observer<Value>]()
//    
//    public var value: Value {
//        didSet { notifyObservers() }
//    }
//    
//    public init(_ value: Value) {
//        self.value = value
//    }
//    
//    public func observe(on observer: AnyObject, observerBlock: @escaping (Value) -> Void) {
//        observers.append(Observer(observer: observer, block: observerBlock))
//        observerBlock(self.value)
//    }
//    
//    public func remove(observer: AnyObject) {
//        observers = observers.filter { $0.observer !== observer }
//    }
//    
//    private func notifyObservers() {
//        for observer in observers {
//            DispatchQueue.main.async { observer.block(self.value) }
//        }
//    }
//}


