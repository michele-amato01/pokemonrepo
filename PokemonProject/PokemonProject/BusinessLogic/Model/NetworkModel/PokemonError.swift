//
//  PokemonError.swift
//  Pokemon
//
//  Created by Michele Amato on 16/11/2020.
//

import Foundation

typealias ErrorClosure = (PokemonError) -> ()

public protocol ErrorProtocol: Swift.Error {}

public enum PokemonError: ErrorProtocol {
    
    //Network
    case invalidURL
    case encodeInput
    case networkError
    case dataResponseNil
    case decodeOutput
    
    case unkown
    
    var description: String {
        return String(describing: self.self)
    }

}
