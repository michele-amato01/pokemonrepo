//
//  Result.swift
//  Pokemon
//
//  Created by Michele Amato on 16/11/2020.
//

import Foundation

public enum Result<Output, CustomError> {
    case success(Output)
    case failure(CustomError)
}
