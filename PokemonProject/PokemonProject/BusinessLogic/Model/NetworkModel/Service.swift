//
//  Service.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import Foundation

struct Service {
    
    var networkService: NetworkServiceProtocol?
    
    init(networkService: NetworkServiceProtocol) {
        self.networkService = networkService
    }
}

extension Service {
    
    static func createService() -> Service {
        let networkService: NetworkService = NetworkService.init()
        let service: Service = Service.init(networkService: networkService)
        return service
    }
}
