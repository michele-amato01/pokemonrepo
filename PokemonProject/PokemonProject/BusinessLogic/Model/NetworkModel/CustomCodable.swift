//
//  CustomCodable.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import Foundation

typealias CustomCodable = PokemonDecodable & Encodable

protocol PokemonDecodable: Decodable {
    static var decoder: JSONDecoder { get }
}

extension PokemonDecodable {
    static var decoder: JSONDecoder  {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }
}
