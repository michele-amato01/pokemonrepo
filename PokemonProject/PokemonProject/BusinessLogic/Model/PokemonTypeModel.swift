//
//  PokemonTypeModel.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import UIKit
import RealmSwift


enum PokemonTypeModel: String {
    
    case normal
    case fighting
    case flying
    case poison
    case ground
    case rock
    case bug
    case ghost
    case steel
    case fire
    case water
    case grass
    case electric
    case psychic
    case ice
    case dragon
    case dark
    case fairy
    case unknown
    case shadow
    
    var backgroundColor: UIColor {
        switch self {
        case .normal:
            return #colorLiteral(red: 0.6579936743, green: 0.6590915322, blue: 0.4715057611, alpha: 1)
        case .fighting:
            return #colorLiteral(red: 0.7526881099, green: 0.1891479492, blue: 0.1574935615, alpha: 1)
        case .flying:
            return #colorLiteral(red: 0.6590029597, green: 0.5640438199, blue: 0.9421505332, alpha: 1)
        case .poison:
            return #colorLiteral(red: 0.6289460063, green: 0.2502722144, blue: 0.628816545, alpha: 1)
        case .ground:
            return #colorLiteral(red: 0.8794056773, green: 0.7540946603, blue: 0.4071154892, alpha: 1)
        case .rock:
            return #colorLiteral(red: 0.7199825644, green: 0.6260414124, blue: 0.2182562649, alpha: 1)
        case .bug:
            return #colorLiteral(red: 0.6576757431, green: 0.7215042114, blue: 0.1267906725, alpha: 1)
        case .ghost:
            return #colorLiteral(red: 0.4402717054, green: 0.3453098536, blue: 0.5951116681, alpha: 1)
        case .steel:
            return #colorLiteral(red: 0.7205502391, green: 0.7207046151, blue: 0.8169816136, alpha: 1)
        case .fire:
            return #colorLiteral(red: 0.9415771365, green: 0.500888586, blue: 0.1902755499, alpha: 1)
        case .water:
            return #colorLiteral(red: 0.4090492427, green: 0.5637253523, blue: 0.9421802163, alpha: 1)
        case .grass:
            return #colorLiteral(red: 0.4707907438, green: 0.7858593464, blue: 0.3154973984, alpha: 1)
        case .electric:
            return #colorLiteral(red: 0.9720122218, green: 0.8148769736, blue: 0.1894631684, alpha: 1)
        case .psychic:
            return #colorLiteral(red: 0.9725164771, green: 0.3431525826, blue: 0.5339694619, alpha: 1)
        case .ice:
            return #colorLiteral(red: 0.5950232744, green: 0.845287025, blue: 0.8485789895, alpha: 1)
        case .dragon:
            return #colorLiteral(red: 0.4377448559, green: 0.2188227773, blue: 0.9729486108, alpha: 1)
        case .dark:
            return #colorLiteral(red: 0.4397318959, green: 0.3462012708, blue: 0.2814345956, alpha: 1)
        case .fairy:
            return #colorLiteral(red: 0.9315437675, green: 0.5995930433, blue: 0.6730148792, alpha: 1)
        case .unknown:
            return #colorLiteral(red: 0.4074895382, green: 0.6289897561, blue: 0.5646819472, alpha: 1)
        case .shadow:
            return #colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1)
        }
    }
}

