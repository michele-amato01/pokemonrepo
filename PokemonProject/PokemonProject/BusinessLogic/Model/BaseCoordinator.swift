//
//  BaseCoordinator.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import UIKit

protocol BaseCoordinator: Coordinator {
    var baseTabBarItem: UITabBarItem { get }
}

