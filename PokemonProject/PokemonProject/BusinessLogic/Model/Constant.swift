//
//  Constant.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import Foundation

struct Url {
    static let baseURL: String = "https://pokeapi.co/api/v2/"
}

