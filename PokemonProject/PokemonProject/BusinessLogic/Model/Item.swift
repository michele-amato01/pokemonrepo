//
//  Item.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import Foundation

final class Item: Codable {
    
    var name: String?
    var url: String?
}
