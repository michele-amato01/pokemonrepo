//
//  Coordinator.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import UIKit

public protocol Coordinator: class {
    var childCoordinators: [Coordinator] { get set }
    var rootViewController: UIViewController { get }
    func start(with option: DeepLinkOption?)
}

 extension Coordinator {
    
    public var presentedCoordinator: Coordinator? {
        return childCoordinators.last
    }
    
    public func add(childCoordinator: Coordinator) {
        guard childCoordinator !== self else { return }
        self.childCoordinators.append(childCoordinator)
    }
    
    public func remove(childCoordinator: Coordinator) {
        self.childCoordinators = self.childCoordinators.filter { $0 !== childCoordinator }
    }
    
    public func search<T: Coordinator>(type: T.Type) -> T? {
        
        if self is T {
            return (self as! T)
        }
        
        for child in childCoordinators {
            if let found = child.search(type: type) {
                return found
            }
        }
        return nil
    }
}
