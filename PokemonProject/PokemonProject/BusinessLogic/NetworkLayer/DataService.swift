//
//  DataService.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import Foundation

protocol NetworkServiceProtocol {
    func perform<Input, Output>(_ request: Request<Input, Output>, completion: @escaping (Result<Output, PokemonError>)->Void)
    func checkHttpStatusCode(_ statusCode: Int) throws
    func manageResponse<Input, Output>(_ response: URLResponse?, data: Data, request: Request<Input, Output>, _ completion: @escaping (Result<Output, PokemonError>)->Void)
}

extension  NetworkServiceProtocol {
    
    func checkHttpStatusCode(_ statusCode: Int) throws { }
    
    func manageResponse<Input, Output>(_ response: URLResponse?, data: Data, request: Request<Input, Output>, _ completion: @escaping (Result<Output, PokemonError>)->Void) {
        
        let statusCode: Int
        statusCode = (response as? HTTPURLResponse)?.statusCode ?? -1
        
        
        do {
            try checkHttpStatusCode(statusCode)
            let output = try JSONDecoder().decode(Output.self, from: data)
            
            print("""
            >> Response for: \(response?.url?.absoluteString ?? request.path)
               status code: \(statusCode)
            \(data.jsonPrettyPrinted ?? "<empty_response>")"
            """)
            completion(.success(output))
            return
        }
        catch {
            completion(.failure(PokemonError.decodeOutput))
            return
        }
    }
}
