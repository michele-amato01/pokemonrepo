//
//  Request.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import Foundation

public struct Request<Input: EncodableHashable, Output: Decodable> {

    let httpMethod: HTTPMethod
    let input: Input
    let customTimeout: TimeInterval?
    var baseURL: String
    var path: String
}

public protocol RequestInfoProtocol {
    static var httpMethod: HTTPMethod { get }
    static var baseURL: String { get }
}

public protocol RequestConvertible: RequestInfoProtocol  {
    associatedtype Input: EncodableHashable
    associatedtype Output: Decodable
    static func request(with input: Input, with path: String) -> Request<Input, Output>
}


public extension RequestConvertible {
    
    static var httpMethod: HTTPMethod {
        return .get
    }
    
    static var customTimeout: TimeInterval? {
        return nil
    }
}

public extension RequestConvertible {
    
    static func request(with input: Input, with path: String) -> Request<Input, Output> {
        
        return Request(httpMethod: Self.httpMethod,
                       input: input,
                       customTimeout: Self.customTimeout,
                       baseURL: Self.baseURL,
                       path: path)
    }
}

extension Request: Hashable {}
