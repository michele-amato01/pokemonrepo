//
//  NetworkService.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import Foundation

public typealias EncodableHashable = Encodable & Hashable

public enum HTTPMethod: String {
    case get     = "GET"
    case post    = "POST"
}

public final class NetworkService: NSObject, NetworkServiceProtocol  {

    private (set) var session: URLSession?
    
    override init() {
        super.init()
        session = initSession()
    }
    
    public func perform<Input, Output>(_ request: Request<Input, Output>, completion: @escaping (Result<Output, PokemonError>) -> Void) where Input : Encodable, Input : Hashable, Output: Decodable {
        
        let encodedInput: Data

        do {
            encodedInput = try JSONEncoder().encode(request.input)
        } catch {
            completion(.failure(PokemonError.encodeInput))
            return
        }

        let urlString: String = request.baseURL + request.path
        
        guard let url = URL(string: urlString) else {
            completion(.failure(PokemonError.invalidURL))
            return
        }
        
        var httpRequest: URLRequest = URLRequest(url: url)
        
        let httpMethod: String = request.httpMethod.rawValue
        httpRequest.httpMethod = httpMethod
        httpRequest.httpBody = nil
        
        if let timeout = request.customTimeout {
            httpRequest.timeoutInterval = timeout
        }
        
        print("""
        >> Request for: \(url)
        \(encodedInput.jsonPrettyPrinted ?? "JSON invalid")
        """)
        
        session?.dataTask(with: httpRequest, completionHandler: { (data, urlResponse, error) in
            
            guard error == nil else {
                completion(.failure(PokemonError.networkError))
                return
            }
            guard let data = data else {
                completion(.failure(PokemonError.dataResponseNil))
                return
            }
                        
            self.manageResponse(urlResponse, data: data, request: request) { (result: Result<Output, PokemonError>) in
                completion(result)
            }
            
        }).resume()
    }
}

extension NetworkService {
    
    private func initSession() -> URLSession {
        let configuration = URLSessionConfiguration.default
        let session: URLSession = URLSession.init(configuration: configuration)
        return session
    }
}

struct DecodableVoid: Decodable {
    init() {}
    init(from decoder: Decoder) throws {}
}
