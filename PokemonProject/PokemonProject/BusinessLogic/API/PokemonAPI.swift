//
//  PokemonAPI.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import Foundation
import RealmSwift

typealias Pokemon = PokemonAPI.Output

struct PokemonAPI: RequestConvertible {
    
    static var baseURL: String {
        return Url.baseURL
    }
    
    struct Input: EncodableHashable { }
    
    struct Output: Decodable {
        var id: Int?
        var name: String?
        var base_experience: Int?
        var height: Int?
        var order: Int?
        var weight: Int?
        var abilities: [PokemonAbility]?
        var forms: [Item]?
        var gameIndices: [VersionGameIndex]?
        var heldItems: [PokemonHeldItem]?
        var locationAreaEncounters: String?
        var moves: [PokemonMove]?
        var sprites: PokemonSprites?
        var stats: [PokemonStat]?
        var types: [PokemonType]?
    }
}

extension Pokemon {
    
    init(name: String) {
        self.name = name
    }
}
