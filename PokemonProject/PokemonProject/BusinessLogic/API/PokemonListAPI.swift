//
//  PokemonListAPI.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import Foundation

struct PokemonListAPI: RequestConvertible {
    
    static var baseURL: String {
        return Url.baseURL
    }
    
    var customTimeout = 30.0
    
    struct Input: EncodableHashable { }
    
    struct Output: Decodable {
        
        var count: Int?
        var next: String?
        var previous: String?
        var current: String = ""
        var results: [Item]?
        
        enum CodingKeys: String, CodingKey {
            case count
            case next
            case previous
            case results
        }
        
        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.count = try container.decodeIfPresent(Int.self, forKey: .count)
            self.next = try container.decodeIfPresent(String.self, forKey: .next)
            self.previous = try container.decodeIfPresent(String.self, forKey: .previous)
            self.results = try container.decodeIfPresent([Item].self, forKey: .results)

        }
    }
}
