//
//  PokemonManager.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import Foundation
import RealmSwift

typealias StartCompletion = (() -> Void)

final class PokemonManager {
    
    //MARK: - Properties
    private let service: Service
    private var offset: Int = 0
    private var limit: Int = 6
        
    //MARK: - Init
    init(service: Service) {
        self.service = service
    }
    
    //MARK: - Methods
    func getPokemonItem(startCompletion: StartCompletion?, onSuccess: @escaping (_ output: [PokemonDetail]) -> Void, onError: @escaping ErrorClosure) {
        
        let input = PokemonListAPI.Input()
        let request = PokemonListAPI.request(with: input, with: "pokemon?limit=\(self.limit)&offset=\(self.offset)")
        
        service.networkService?.perform(request) { result in
            switch result {
            case .success(let output):
                
                if let results = output.results {
                    
                    self.offset += self.limit
                    
                    var list: [String] = []
                    results.forEach { item in
                        if let name = item.name {
                            list.append(name)
                        }
                    }
                    self.getPokemonDetails(list) {
                        startCompletion?()
                    } onSuccess: { pokemon in
//                        self.savePokemon(pokemon)
                        onSuccess(pokemon)
                    } onError: { error in
                        onError(error)
                    }
                    
                } else {
                    onSuccess([])
                }
            case .failure(let error):
                onError(error)
            }
        }
    }
    
    func getPokemonDetails(_ pokemonNames: [String], startCompletion: StartCompletion?, onSuccess: @escaping (_ output: [PokemonDetail]) -> Void, onError: @escaping ErrorClosure) {
        
        var pokemonList: [PokemonDetail] = []
        var names: [String] = []
        
        let dispatchGroup = DispatchGroup()
        let dispatchQueue = DispatchQueue(label: "taskQueue")
        let dispatchSemaphore = DispatchSemaphore(value: 0)
        
        dispatchQueue.async { [weak self] in
            
            for pokemonName in pokemonNames {
                
                dispatchGroup.enter()
                self?.getPokemon(pokemonName, startCompletion: startCompletion) { pokemon in
                    
                    pokemonList.append(pokemon)
                    names.append(pokemon.name)
                    dispatchSemaphore.signal()
                    dispatchGroup.leave()
                } onError: { error in
                    dispatchSemaphore.signal()
                    dispatchGroup.leave()
                }
                
                dispatchSemaphore.wait()
            }
            
            dispatchGroup.notify(queue: .main, work: DispatchWorkItem(block: {
                onSuccess(pokemonList)
            }))
        }
    }
    
    
    func getPokemon(_ pokemonName: String, startCompletion: StartCompletion?, onSuccess: @escaping (_ output: PokemonDetail) -> Void,  onError: @escaping ErrorClosure) {
        
        startCompletion?()
        
        if let pokemonDetail = getPokemonFromRealm(pokemonName) {
            onSuccess(pokemonDetail)
        } else {
            let input = PokemonAPI.Input()
            let request = PokemonAPI.request(with: input, with: "pokemon/\(pokemonName)")
            
            service.networkService?.perform(request) { result in
                switch result {
                case .success(let output):
                    let pokemonDetail =  PokemonDetail.init(from: output)
                    //self.savePokemonInRealm(pokemon: pokemonDetail)
                    onSuccess(pokemonDetail)
                case .failure(let error):
                    onError(error)
                }
            }
        }
    }
    
    
    private func savePokemon(_ allPokemon: [PokemonDetail]) {
        
        allPokemon.forEach { pokemon in
            self.savePokemonInRealm(pokemon: pokemon, completion: nil)
        }
    }
    
    
    private func getPokemonFromRealm(_ pokemonName: String) -> PokemonDetail? {
        
        return nil
//
//        var realm: Realm?
//        do {
//            realm = try Realm.init()
//        } catch {
//            return nil
//        }
//
//        if let pokemonDetail = realm?.object(ofType: PokemonDetail.self, forPrimaryKey: pokemonName) {
//
//            realm = nil
//            return pokemonDetail
//
//        }
//        return nil
    }
    
    
    private func savePokemonInRealm(pokemon: PokemonDetail, completion: (() -> Void)? = nil) {

        let realm: Realm?
        
        do {
            realm = try Realm.init()
        } catch {
            completion?()
            return
        }
        
        do {
            try realm?.write {(
                realm?.add(pokemon, update: .modified),
                completion?()
            )}
        } catch {
            completion?()
            
            return
        }
    }
    
}
