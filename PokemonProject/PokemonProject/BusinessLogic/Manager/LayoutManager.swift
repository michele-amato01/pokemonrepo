//
//  LayoutManager.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import UIKit

final class LayoutManager {
        
    static var pokemonImageHeight: CGFloat {
        return round(UIScreen.main.bounds.height/4)
    }
    
    static var pokemonImagesHeight: CGFloat {
        return round(UIScreen.main.bounds.height/8)
    }

    static var headerHeight: CGFloat {
        return round(UIScreen.main.bounds.height/12)
    }
    
}
