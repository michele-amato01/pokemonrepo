//
//  DataExtension.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import Foundation

extension Data {
    var jsonPrettyPrinted: String? {
        guard let jsonObject: Any = try? JSONSerialization.jsonObject(with: self, options: .allowFragments) else {
            return nil
        }
        guard let jsonData: Data = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
            return nil
        }
        guard let jsonString: String = String(data: jsonData, encoding: .utf8) else {
            return nil
        }
        return jsonString
    }
}

