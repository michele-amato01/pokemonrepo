//
//  InterfaceBuilder.swift
//  Pokemon
//
//  Created by Michele Amato on 18/11/2020.
//

import UIKit

// MARK: - UITableView
public extension UITableView {
    
    final func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath, cellIdentifier cellType: ListIdentifier) -> T {
        guard let cell = self.dequeueReusableCell(withIdentifier: cellType.name, for: indexPath) as? T else {
            preconditionFailure("Failed to dequeue a cell with identifier \(cellType.name) matching type \(cellType). Check that the reuseIdentifier is set properly in your XIB/Storyboard and that you registered the cell for reuse.")
        }
        return cell
    }

}

// MARK: - UICollectionView
extension UICollectionView {
    
    final public func dequeueReusableCell<T: UICollectionViewCell>(for indexPath: IndexPath, cellIdentifier cellType: ListIdentifier) -> T {
        let cell = self.dequeueReusableCell(withReuseIdentifier: cellType.name, for: indexPath)
        guard let typedCell = cell as? T else {
            preconditionFailure("Failed to dequeue a cell with identifier \(cellType.name) matching type \(cellType). Check that the reuseIdentifier is set properly in your XIB/Storyboard and that you registered the cell for reuse.")
        }
        return typedCell
    }
    
}
