//
//  UIImageViewExtension.swift
//  Pokemon
//
//  Created by Michele Amato on 28/11/2020.
//

import UIKit

let imageCache = NSCache<NSString, UIImage>()

extension UIImageView {

    func load(_ urlString: String, placeHolder: UIImage?) {

        self.image = nil
        if let cachedImage = imageCache.object(forKey: NSString(string: urlString)) {
            self.image = cachedImage
            return
        }

        if let url = URL(string: urlString) {
            URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in

                if error != nil {
                    DispatchQueue.main.async { [weak self] in
                        self?.image = placeHolder
                    }
                    return
                }
                DispatchQueue.main.async { [weak self] in
                    if let data = data {
                        if let downloadedImage = UIImage(data: data) {
                            imageCache.setObject(downloadedImage, forKey: NSString(string: urlString))
                            self?.image = downloadedImage
                        }
                    }
                }
            }).resume()
        }
    }
}
