//
//  UIViewExtension.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import UIKit

extension UIView {
    static var reuseIdentifier: String {
        return String(describing: self.self)
    }
}

