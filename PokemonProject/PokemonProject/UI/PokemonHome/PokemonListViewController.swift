//
//  PokemonListViewController.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import UIKit

protocol PokemonListViewControllerDelegate: class {
    func openPokemonDetail(_ pokemonListViewController: PokemonListViewController, for pokemon: PokemonDetail)
}

final class PokemonListViewController: UIViewController {
    
    //MARK: - Properties
    private let columns: CGFloat = 3.0
    private let inset: CGFloat = 8.0
    private let spacing: CGFloat = 8.0
    private let lineSpacing: CGFloat = 8.0
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 0.0
        layout.minimumLineSpacing = 0.0
        return UICollectionView(frame: .zero, collectionViewLayout: layout)
    }()

    private lazy var errorView: ErrorView = {
        let view = ErrorView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    weak var delegate: PokemonListViewControllerDelegate?
    private var viewModel: PokemonViewModel!
    
    //MARK: - View Lifecycle
    convenience init(viewModel: PokemonViewModel) {
        self.init()
        self.viewModel = viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    //MARK: - Methods
    private func setup() {
        self.setupUI()
        self.setupData()
    }
    
    private func setupUI() {
        self.view.backgroundColor = UIColor.white
        
        self.view.addSubview(collectionView)
        collectionView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = UIColor.clear
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.register(PokemonCell.self, forCellWithReuseIdentifier: PokemonCell.reuseIdentifier)
        collectionView.register(LoadMoreCell.self, forCellWithReuseIdentifier: LoadMoreCell.reuseIdentifier)
        
        self.view.addConstraint( NSLayoutConstraint(item: collectionView, attribute: .leading, relatedBy: .equal, toItem: self.view.safeAreaLayoutGuide, attribute: .leading, multiplier: 1, constant: 0))
        self.view.addConstraint( NSLayoutConstraint(item: collectionView, attribute: .top, relatedBy: .equal, toItem: self.view.safeAreaLayoutGuide, attribute: .top, multiplier: 1, constant: 0))
        self.view.addConstraint( NSLayoutConstraint(item: self.view.safeAreaLayoutGuide, attribute: .trailing, relatedBy: .equal, toItem: collectionView, attribute: .trailing, multiplier: 1, constant: 0))
        self.view.addConstraint( NSLayoutConstraint(item: self.view.safeAreaLayoutGuide, attribute: .bottom, relatedBy: .equal, toItem: collectionView, attribute: .bottom, multiplier: 1, constant: 0))
        
        self.view.addSubview(errorView)
        NSLayoutConstraint.activate([
            self.errorView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            self.errorView.topAnchor.constraint(equalTo: self.view.topAnchor),
            self.errorView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            self.errorView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        ])
    }
    
    private func setupData() {
        viewModel.pokemonList.bind { [weak self] pokemonList  in
            DispatchQueue.main.async {
                self?.collectionView.reloadData()
                self?.collectionView.isHidden = false
                self?.errorView.isHidden = true
            }
        }
        
        self.viewModel.error.bind({[weak self] error in
            if !error.isEmpty {
                DispatchQueue.main.async {
                    self?.collectionView.isHidden = true
                    self?.errorView.isHidden = false
                }
            }
        })
    }
}


// MARK: - UICollectionViewDataSource
extension PokemonListViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.viewModel.pokemonList.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.pokemonList.value[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let vmItem = self.viewModel.pokemonList.value[indexPath.section][indexPath.row]
        let cell = collectionView.dequeueReusableCell(for: indexPath, cellIdentifier: vmItem.itemIdentifier)
        (cell as? ViewModelBindable)?.bind(to: vmItem)
        return cell
    }
}

// MARK: - UICollectionViewDelegate
extension PokemonListViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.viewModel.pokemonList.value[indexPath.section][indexPath.row] is PokemonListViewModel {
            self.delegate?.openPokemonDetail(self, for: self.viewModel.pokemonDetailList[indexPath.row])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if self.viewModel.pokemonList.value[indexPath.section][indexPath.row] is LoadMoreViewModel {
            self.viewModel.getPokemonList()
        }
    }
}


// MARK: UICollectionViewDelegateFlowLayout
extension PokemonListViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if self.viewModel.pokemonList.value[indexPath.section][indexPath.row] is LoadMoreViewModel {
            let totalSpace: CGFloat = (inset * 2 ) + (spacing * (1 - 1))
            let availableWidth: CGFloat = (collectionView.bounds.width - totalSpace)
            let sizeFloat: CGFloat = availableWidth/columns
            let size: Int = Int(sizeFloat)
            return CGSize(width: Int(availableWidth), height: size)
        } else {
            
            let totalSpace: CGFloat = (inset * 2 ) + (spacing * (columns - 1))
            let availableWidth: CGFloat = (collectionView.bounds.width - totalSpace)
            let sizeFloat: CGFloat = availableWidth/columns
            let size: Int = Int(sizeFloat)
            return CGSize(width: size, height: size)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: inset, left: inset, bottom: inset, right: inset)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return spacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return lineSpacing
    }
}
