//
//  PokemonInfoCell.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import UIKit

final class PokemonInfoCell: UITableViewCell, ViewModelBindable {
    
    private enum Constants {
        static let columns: CGFloat = 3.0
        static let inset: CGFloat = 8.0
        static let spacing: CGFloat = 8.0
        static let lineSpacing: CGFloat = 8.0
    }
    
    //MARK: - Properties
    var dataSource: [ItemValue] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    
    private var shouldResizeCollectionView: Bool = true
    
    private (set) lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = Constants.spacing
        layout.minimumLineSpacing = Constants.lineSpacing
        return UICollectionView(frame: .zero, collectionViewLayout: layout)
    }()
    
    
    //MARK: - View Lifecycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    private func setup() {
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .white
        collectionView.register(InformationCell.self, forCellWithReuseIdentifier: InformationCell.reuseIdentifier)
        self.selectionStyle = .none
        self.addSubview(self.collectionView)
        let collectionView = self.collectionView as Any
        
        addConstraint( NSLayoutConstraint(item: collectionView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: collectionView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: collectionView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: collectionView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0))
    }
    
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        if shouldResizeCollectionView {
            let maxSize: CGSize = CGSize(width: targetSize.width, height: 100000)
            collectionView.frame = CGRect(origin: CGPoint.zero, size: maxSize)
            collectionView.layoutIfNeeded()
        }
        
        shouldResizeCollectionView = false
        
        let collectionViewSize: CGSize = collectionView.collectionViewLayout.collectionViewContentSize
        let height: CGFloat = collectionViewSize.height
        return CGSize(width: collectionViewSize.width, height: height)
    }
    
    //MARK: - Methods
    func bind(to viewModel: ViewModel) {
        guard let vm = viewModel as? PokemonInfoViewModel else {
            return
        }
        self.dataSource = vm.itemValue
    }
    
}


//MARK: -  UICollectionViewDataSource
extension PokemonInfoCell: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: InformationCell = collectionView.dequeueReusableCell(withReuseIdentifier: InformationCell.reuseIdentifier, for: indexPath) as? InformationCell else { return InformationCell() }
        cell.configure(with: self.dataSource[indexPath.row])
        return cell
    }
}


// MARK: UICollectionViewDelegateFlowLayout
extension PokemonInfoCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let totalSpace: CGFloat = (Constants.inset * 2 ) + (Constants.spacing * (Constants.columns - 1))
        let availableWidth: CGFloat = (collectionView.bounds.width - totalSpace)
        let sizeFloat: CGFloat = availableWidth/Constants.columns
        let size: Int = Int(sizeFloat)
        return CGSize(width: size, height: size)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: Constants.inset, bottom: 0, right: Constants.inset)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return Constants.spacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return Constants.lineSpacing
    }
}
