//
//  PokemonCell.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import UIKit

final class PokemonCell: UICollectionViewCell, ViewModelBindable {
    
    //MARK: - Properties
    private enum Constants {
        static let font: UIFont = UIFont.systemFont(ofSize: 14.0, weight: .bold)
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Constants.font
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.black
        return label
    }()
    
    private lazy var pokemonImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.image = #imageLiteral(resourceName: "pokemonLogo")
        return imageView
    }()
    
    //MARK: - View Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    private func setup() {
        self.backgroundColor = .white
        self.addSubview(self.pokemonImageView)
        self.addSubview(self.titleLabel)
        
        addConstraint( NSLayoutConstraint(item: pokemonImageView, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: 0))
        addConstraint( NSLayoutConstraint(item: pokemonImageView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: 0))
        addConstraint( NSLayoutConstraint(item: self.contentView, attribute: .trailing, relatedBy: .equal, toItem: pokemonImageView, attribute: .trailing, multiplier: 1, constant: 0))
        
        addConstraint( NSLayoutConstraint(item: titleLabel, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: 0))
        addConstraint( NSLayoutConstraint(item: titleLabel, attribute: .top, relatedBy: .equal, toItem: self.pokemonImageView, attribute: .bottom, multiplier: 1, constant: 0))
        addConstraint( NSLayoutConstraint(item: self.contentView, attribute: .trailing, relatedBy: .equal, toItem: titleLabel, attribute: .trailing, multiplier: 1, constant: 0))
        addConstraint( NSLayoutConstraint(item: self.contentView, attribute: .bottom, relatedBy: .equal, toItem: titleLabel, attribute: .bottom, multiplier: 1, constant: 0)) 
    }
    
    //MARK: - Methods
    func bind(to viewModel: ViewModel) {
        guard let vm = viewModel as? PokemonListViewModel else {
            return
        }
        self.pokemonImageView.load(vm.imageURLString, placeHolder: #imageLiteral(resourceName: "pokemonLogo"))
        self.titleLabel.text = vm.title.capitalized
    }
}
