//
//  PokemonTypeCell.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import UIKit

final class PokemonTypeCell: UITableViewCell, ViewModelBindable {
    
    //MARK: - Properties
    private var shouldResizeCollectionView: Bool = true
    
    private lazy var pokemonTypeView: PokemonTypeView = {
        let view = PokemonTypeView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    //MARK: - View Lifecycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
          super.init(style: style, reuseIdentifier: reuseIdentifier)
          self.setup()
      }
      
      required init?(coder aDecoder: NSCoder) {
          super.init(coder: aDecoder)
          self.setup()
      }
    
    private func setup() {
        self.backgroundColor = .white
        self.selectionStyle = .none
        self.addSubview(self.pokemonTypeView)
        NSLayoutConstraint.activate([
            self.pokemonTypeView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.pokemonTypeView.topAnchor.constraint(equalTo: self.topAnchor),
            self.pokemonTypeView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.pokemonTypeView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }
    
    //MARK: - Methods
    func bind(to viewModel: ViewModel) {
        guard let vm = viewModel as? PokemonTypeViewModel else {
            return
        }
        self.pokemonTypeView.types = vm.pokemonType
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        if shouldResizeCollectionView {
            let maxSize: CGSize = CGSize(width: targetSize.width, height: 100000)
            pokemonTypeView.collectionView.frame = CGRect(origin: CGPoint.zero, size: maxSize)
            pokemonTypeView.collectionView.layoutIfNeeded()
        }
        
        shouldResizeCollectionView = false
        
        let collectionViewSize: CGSize = pokemonTypeView.collectionView.collectionViewLayout.collectionViewContentSize
        let height: CGFloat = collectionViewSize.height + 30.0
        return CGSize(width: collectionViewSize.width, height: height)
    }
    
}
