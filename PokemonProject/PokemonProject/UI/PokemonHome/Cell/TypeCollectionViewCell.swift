//
//  TypeCollectionViewCell.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import UIKit

final class TypeCollectionViewCell: UICollectionViewCell {
    
    //MARK: - Properties
    private enum Constants {
        static let font: UIFont = UIFont.systemFont(ofSize: 12.0, weight: .bold)
        static let paddingX: CGFloat = 8
        static let paddingY: CGFloat = 10
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Constants.font
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.white
        return label
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    
    //MARK: - Properties
    weak var collectionView: UICollectionView?
    var indexPath: IndexPath?
    
        
    //MARK: - View Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    private func setup() {
        
        self.containerView.backgroundColor = .green
        self.addSubview(self.containerView)
        
        NSLayoutConstraint.activate([
            self.containerView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.containerView.topAnchor.constraint(equalTo: self.topAnchor),
            self.containerView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.containerView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
        
        self.containerView.addSubview(self.titleLabel)
        NSLayoutConstraint.activate([
            titleLabel.centerXAnchor.constraint(equalTo: self.containerView.centerXAnchor),
            titleLabel.centerYAnchor.constraint(equalTo: self.containerView.centerYAnchor),
      ])
    }
    
    //MARK: - Methods
    func config(with type: PokemonTypeModel) {
        titleLabel.text = type.rawValue.uppercased()
        containerView.backgroundColor = type.backgroundColor
    }
}


extension TypeCollectionViewCell {
    
    static func getSize(for text: String) -> CGSize {
        let fontLabel: UIFont = Constants.font
        let size = text.uppercased().size(withAttributes: [NSAttributedString.Key.font: fontLabel])
        let widthText: CGFloat = size.width
        let heightText: CGFloat = size.height
        let width: CGFloat = widthText + (Constants.paddingX * 2)
        let height: CGFloat = heightText + (Constants.paddingY * 2)
        return CGSize.init(width: width, height: height)
    }
}
