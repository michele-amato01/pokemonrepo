//
//  LoadMoreCell.swift
//  Pokemon
//
//  Created by Michele Amato on 28/11/2020.
//

import UIKit

final class LoadMoreCell: UICollectionViewCell, ViewModelBindable {
    
    //MARK: - Properties
    private enum Constants {
        static let font: UIFont = UIFont.systemFont(ofSize: 16.0, weight: .bold)
    }
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.style = .large
        activityIndicator.color = .red
        return activityIndicator
    }()
    
    //MARK: - View Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    private func setup() {
        self.backgroundColor = .white
        self.addSubview(self.activityIndicator)
        
        addConstraint(NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 100))
        addConstraint(NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 100))
        addConstraint(NSLayoutConstraint(item: activityIndicator, attribute: .centerX, relatedBy: .equal, toItem: self.contentView, attribute: .centerX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: activityIndicator, attribute: .centerY, relatedBy: .equal, toItem: self.contentView, attribute: .centerY, multiplier: 1, constant: 0))
    
    }
    
    //MARK: - Methods
    func bind(to viewModel: ViewModel) {
        guard let _ = viewModel as? LoadMoreViewModel else {
            return
        }
        animating()
    }
    
    private func animating() {
        if !activityIndicator.isAnimating {
            activityIndicator.startAnimating()
        }
    }
    
}
