//
//  InformationCell.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import UIKit

final class InformationCell: UICollectionViewCell {
    
    //MARK: - Properties
    private enum Constants {
        static let descriptionLabelFont: UIFont = UIFont.systemFont(ofSize: 18.0, weight: .bold)
        static let valueLabelFont: UIFont = UIFont.systemFont(ofSize: 16.0, weight: .bold)
    }

    //MARK: - Properties
    private lazy var itemLabel: UILabel = {
        let label = UILabel()
        label.font = Constants.descriptionLabelFont
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.black
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.font = Constants.valueLabelFont
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.black
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.alignment = .center
        stackView.spacing = 5.0
        return stackView
    }()
      
    //MARK: - View Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    private func setup() {
        self.backgroundColor = .white
        self.addSubview(self.stackView)
        
        self.stackView.addSubview(self.itemLabel)
        self.stackView.addSubview(self.valueLabel)

        NSLayoutConstraint.activate([
            self.stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.stackView.topAnchor.constraint(equalTo: self.topAnchor),
            self.stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])

        self.stackView.addArrangedSubview(self.itemLabel)
        self.stackView.addArrangedSubview(self.valueLabel)
    }
    
    func configure(with itemValue: ItemValue) {
        self.itemLabel.text = itemValue.item
        self.valueLabel.text = itemValue.value
    }
}
