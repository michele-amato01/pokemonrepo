//
//  KeyValueCell.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import UIKit

final class KeyValueCell: UITableViewCell, ViewModelBindable {
    
    private enum Constants {
        static let font: UIFont = UIFont.systemFont(ofSize: 15.0, weight: .bold)
        static let padding: CGFloat = 10
    }
    
    //MARK: - Properties
    private lazy var itemLabel: UILabel = {
        let label = UILabel()
        label.font = Constants.font
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.white
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.font = Constants.font
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.white
        
        return label
    }()
    
    //MARK: - View Lifecycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    override func prepareForReuse() {
        self.itemLabel.text = ""
        self.valueLabel.text = ""
    }
    
    //MARK: - Methods
    private func setup() {
        self.backgroundColor = .red
        self.selectionStyle = .none
        self.addSubview(self.itemLabel)
        
        addConstraint( NSLayoutConstraint(item: itemLabel, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: Constants.padding))
        addConstraint( NSLayoutConstraint(item: itemLabel, attribute: .top, relatedBy: .greaterThanOrEqual, toItem: self, attribute: .top, multiplier: 1, constant: Constants.padding))
        addConstraint( NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .greaterThanOrEqual, toItem: itemLabel, attribute: .bottom, multiplier: 1, constant: Constants.padding))
        
        self.addSubview(self.valueLabel)
        
        addConstraint( NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: valueLabel, attribute: .trailing, multiplier: 1, constant: Constants.padding))
        addConstraint( NSLayoutConstraint(item: valueLabel, attribute: .top, relatedBy: .greaterThanOrEqual, toItem: self, attribute: .top, multiplier: 1, constant: Constants.padding))
        addConstraint( NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .greaterThanOrEqual, toItem: valueLabel, attribute: .bottom, multiplier: 1, constant: Constants.padding))
        
        addConstraint( NSLayoutConstraint(item: valueLabel, attribute: .leading, relatedBy: .greaterThanOrEqual, toItem: itemLabel, attribute: .trailing, multiplier: 1, constant: Constants.padding))
    }
    
    func bind(to viewModel: ViewModel) {
        guard let vm = viewModel as? KeyValueViewModel else {
            return
        }
        self.itemLabel.text = vm.item
        self.valueLabel.text = vm.value
    }
    
}
