//
//  PokemonImageCell.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import UIKit

final class PokemonImageCell: UITableViewCell, ViewModelBindable {

    private enum Constants {
        static let paddingY: CGFloat = 15
    }

    //MARK: - Properties
    private lazy var pokemonImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    //MARK: - View Lifecycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }

    private func setup() {
        self.selectionStyle = .none
        self.backgroundColor = .white
        self.addSubview(self.pokemonImageView)
        NSLayoutConstraint.activate([
            self.pokemonImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.pokemonImageView.topAnchor.constraint(equalTo: self.topAnchor, constant: Constants.paddingY),
            self.pokemonImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.pokemonImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -Constants.paddingY),
            self.pokemonImageView.heightAnchor.constraint(equalToConstant: LayoutManager.pokemonImageHeight)
        ])
    }

    //MARK: - Methods
    func bind(to viewModel: ViewModel) {
        guard let vm = viewModel as? PokemonImageViewModel else {
            return
        }
        self.pokemonImageView.load(vm.defaultImage, placeHolder: #imageLiteral(resourceName: "pokemonLogo"))
    }
}
