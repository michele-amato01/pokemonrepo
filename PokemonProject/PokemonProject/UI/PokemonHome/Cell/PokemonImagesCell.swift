//
//  PokemonImagesCell.swift
//  Pokemon
//
//  Created by Michele Amato on 29/11/2020.
//

import UIKit

final class PokemonImagesCell: UITableViewCell, ViewModelBindable {
    
    private enum Constants {
        static let paddingY: CGFloat = 15
    }
    
    //MARK: - Properties
    private lazy var pokemonFrontImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var pokemonBackImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        stackView.alignment = .center
        stackView.spacing = 5.0
        return stackView
    }()
    
    //MARK: - View Lifecycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    private func setup() {
        self.selectionStyle = .none
        self.backgroundColor = .white

        self.addSubview(self.stackView)
        
        self.stackView.addSubview(self.pokemonFrontImageView)
        self.stackView.addSubview(self.pokemonBackImageView)

        NSLayoutConstraint.activate([
            self.stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.stackView.topAnchor.constraint(equalTo: self.topAnchor, constant: Constants.paddingY),
            self.stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -Constants.paddingY),
            self.stackView.heightAnchor.constraint(equalToConstant: LayoutManager.pokemonImagesHeight)
        ])

        self.stackView.addArrangedSubview(self.pokemonFrontImageView)
        self.stackView.addArrangedSubview(self.pokemonBackImageView)
    }
    
    //MARK: - Methods
    func bind(to viewModel: ViewModel) {
        guard let vm = viewModel as? PokemonImagesViewModel else {
            return
        }
        self.pokemonFrontImageView.load(vm.frontImage, placeHolder: #imageLiteral(resourceName: "pokemonLogo"))
        self.pokemonBackImageView.load(vm.backImage, placeHolder: #imageLiteral(resourceName: "pokemonLogo"))

    }
}

