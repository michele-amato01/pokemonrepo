//
//  HeaderView.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import UIKit

final class HeaderView: UIView {
    
    private enum Constants {
        static let font: UIFont = UIFont.systemFont(ofSize: 20.0, weight: .bold)
    }
   
    //MARK: - Properties
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Constants.font
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.black
        return label
    }()

    //MARK: - View Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Methods
    private func setup() {
        self.backgroundColor = UIColor.white
        addSubview(titleLabel)
        titleLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
    
    func configure(with title: String) {
        titleLabel.text = title
    }

}
