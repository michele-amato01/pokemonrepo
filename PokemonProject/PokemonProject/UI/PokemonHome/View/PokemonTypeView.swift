//
//  PokemonTypeView.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import UIKit

final class PokemonTypeView: UIView {
    
    private enum Constants {
        static let paddingY: CGFloat = 15
    }

    //MARK: - Properties
    private (set) lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 0.0
        layout.minimumLineSpacing = 0.0
        return UICollectionView(frame: .zero, collectionViewLayout: layout)
    }()
        
    var types: [PokemonTypeModel] = [] {
        didSet {
            collectionView.reloadData()
        }
    }

    private var heightConstraint: NSLayoutConstraint?
    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    deinit {
        collectionView.removeObserver(self, forKeyPath: #keyPath(UICollectionView.contentSize), context: nil)
    }
    
    //MARK: - Methods
    private func setup() {
        setupCollectionView()
        setupConstraints()
        setupObserver()
    }
    
    private func setupConstraints() {
        autoresizingMask = [.flexibleHeight, .flexibleWidth]
        let collectionView = self.collectionView as Any
        addConstraint( NSLayoutConstraint(item: collectionView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: collectionView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: collectionView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: Constants.paddingY))
        let bottomConstraint = NSLayoutConstraint(item: collectionView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: Constants.paddingY)
        bottomConstraint.priority = UILayoutPriority(rawValue: 999)
        addConstraint(bottomConstraint)
        let heightConstraint = NSLayoutConstraint(item: collectionView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
        self.heightConstraint = heightConstraint
        addConstraint(heightConstraint)
    }
        
    
    private func setupCollectionView() {
        
        collectionView = UICollectionView(frame: bounds, collectionViewLayout: CenterAlignedCollectionViewFlowLayout())
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        collectionView.register(TypeCollectionViewCell.self, forCellWithReuseIdentifier: TypeCollectionViewCell.reuseIdentifier)
        collectionView.backgroundColor = .white
        collectionView.reloadData()
        addSubview(collectionView)
    }
    
    private func setupObserver() {
        collectionView.addObserver(self, forKeyPath: #keyPath(UICollectionView.contentSize), options: [.new, .old], context: nil)
        collectionView.contentInset = UIEdgeInsets(top: 0 , left: 0, bottom: 0, right: 0)
    }
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if let change = change,
            let oldSize: CGSize = change[NSKeyValueChangeKey.oldKey] as? CGSize,
            let newSize: CGSize = change[NSKeyValueChangeKey.newKey] as? CGSize {
            if oldSize != newSize {
                updateHeightConstraint()
            }
        }
    }
    
    fileprivate func updateHeightConstraint() {
        let collectionViewContentSize = collectionView.contentSize
        let height = collectionViewContentSize.height + collectionView.contentInset.top + collectionView.contentInset.bottom
        heightConstraint?.constant = height
    }
    
}

//MARK: - UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension PokemonTypeView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return types.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell: TypeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: TypeCollectionViewCell.reuseIdentifier, for: indexPath) as? TypeCollectionViewCell else { return UICollectionViewCell() }
        let type = types[indexPath.item]
        cell.config(with: type)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return TypeCollectionViewCell.getSize(for: types[indexPath.row].rawValue)
    }
}
