//
//  PokemonViewModel.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import Foundation
import UIKit

final class PokemonViewModel: ViewModelProtocol {
    
    //MARK: - Properties
    var service: Service
    private let pokemonManager: PokemonManager
    
    var pokemonList: Observable<[[ItemViewModel]]> = Observable([])
    var pokemonDetailList: [PokemonDetail] = []
    var error: Observable<String> = Observable("")
    private var allPokemon: [PokemonListViewModel] = []
    
    //MARK: - Init
    init(service: Service) {
        self.service = service
        self.pokemonManager = PokemonManager.init(service: service)
        self.getPokemonList()
    }
    
    //MARK: - Methods
    func getPokemonList() {
    
        self.pokemonManager.getPokemonItem {
            
        } onSuccess: {[weak self] pokemonList in
            
            guard let self = self else { return }
            var titles: [PokemonListViewModel] = []
            var list: [String] = []
            pokemonList.forEach { pokemon in
                titles.append(PokemonListViewModel.init(title: pokemon.name, imageURLString: pokemon.defaultImage))
                list.append(pokemon.name)
            }
            self.pokemonDetailList.append(contentsOf: pokemonList)
            self.allPokemon.append(contentsOf: titles)

            var viewModel: [[ItemViewModel]]  = []
            viewModel.append(self.allPokemon)

            if titles.count > 0 {
                let loadMoreViewModel = LoadMoreViewModel.init()
                viewModel.append([loadMoreViewModel])
            }
            
            self.pokemonList.value = viewModel

        } onError: { error in
            self.error.value = error.description
        }
     }
}
