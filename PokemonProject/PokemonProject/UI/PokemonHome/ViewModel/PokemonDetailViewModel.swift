//
//  PokemonDetailViewModel.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import Foundation
import RealmSwift

final class PokemonDetailViewModel: ViewModelProtocol {
    
    //MARK: - Properties
     var pokemonDetailViewModel: Observable<[(title:String, items:[ItemViewModel])]>
    private var pokemon: PokemonDetail

    //MARK: - Init
    init(pokemon: PokemonDetail) {
        self.pokemonDetailViewModel = Observable([])
        self.pokemon = pokemon
        self.getPokemonInfo()
    }
    
    //MARK: - Methods
    func getPokemonInfo() {
        self.pokemonDetailViewModel.value = self.getViewModel(from: self.pokemon)
    }

    private func getViewModel(from pokemon: PokemonDetail) -> [(title: String, items: [ItemViewModel])]  {

        var viewModel: [(title: String, items: [ItemViewModel])]  = []

        // Pokemon Image
        viewModel.append((title: "", items: [PokemonImageViewModel.init(defaultImage: pokemon.defaultImage)]))
        viewModel.append((title: "", items: [PokemonImagesViewModel.init(frontImage: pokemon.frontImage, backImage: pokemon.backImage)]))
        
        // Pokemon Info
        let pokemonInfoViewModel = PokemonInfoViewModel()
        pokemon.informations.forEach { itemValue in
            pokemonInfoViewModel.itemValue.append(itemValue)
        }
        viewModel.append((title: "", [pokemonInfoViewModel]))
        
        // Pokemon Types
        var types: [PokemonTypeModel] = []
        pokemon.types.forEach { name in
            types.append(PokemonTypeModel.init(rawValue: name) ?? PokemonTypeModel.unknown)
        }
        
        viewModel.append((title: "TYPE", [PokemonTypeViewModel.init(pokemonType: types)]))
        
        // Pokemon Abilities

        var abilities: [KeyValueViewModel] = []
        pokemon.abilities.forEach { item in
            abilities.append(KeyValueViewModel.init(item: item))
        }
        viewModel.append((title: "ABILITY", abilities))

        // Pokemon Stats
        var stats: [KeyValueViewModel] = []
        pokemon.stats.forEach { itemValue in
            stats.append(KeyValueViewModel.init(item: itemValue.item, value: itemValue.value))
        }
        viewModel.append((title: "STATS", stats))
        
        return viewModel
    }
}
