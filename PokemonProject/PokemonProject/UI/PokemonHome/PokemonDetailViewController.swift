//
//  PokemonDetailViewController.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import UIKit

final class PokemonDetailViewController: UIViewController {
    
    //MARK: - Properties
    private lazy var tableView: UITableView = {
        var tableView: UITableView = UITableView(frame: .zero, style: .insetGrouped)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = 150
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.white
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    private var viewModel: PokemonDetailViewModel!
    
    //MARK: - View Lifecycle
    convenience init(viewModel: PokemonDetailViewModel) {
        self.init()
        self.viewModel = viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    
    //MARK: - Methods
    private func setup() {
        self.setupUI()
        self.setupData()
    }
    
    private func setupUI() {
        self.view.backgroundColor = UIColor.white
        self.setupTableView()
    }
    
    private func setupTableView() {
        tableView.backgroundColor = UIColor.white
        tableView.sectionHeaderHeight = 0.0
        self.view.addSubview(tableView)
        tableView.register(PokemonImageCell.self, forCellReuseIdentifier: PokemonImageCell.reuseIdentifier)
        tableView.register(PokemonImagesCell.self, forCellReuseIdentifier: PokemonImagesCell.reuseIdentifier)
        tableView.register(PokemonTypeCell.self, forCellReuseIdentifier: PokemonTypeCell.reuseIdentifier)
        tableView.register(PokemonInfoCell.self, forCellReuseIdentifier: PokemonInfoCell.reuseIdentifier)
        tableView.register(KeyValueCell.self, forCellReuseIdentifier: KeyValueCell.reuseIdentifier)
        NSLayoutConstraint.activate([
            self.tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            self.tableView.topAnchor.constraint(equalTo: self.view.topAnchor),
            self.tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            self.tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        ])
    }
    
    private func setupData() {
        self.viewModel.pokemonDetailViewModel.bind({[weak self] pokemon in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        })
    }
    
}

//MARK: - UITableViewDataSource
extension PokemonDetailViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.pokemonDetailViewModel.value.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.pokemonDetailViewModel.value[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let vmItem = self.viewModel.pokemonDetailViewModel.value[indexPath.section].items[indexPath.row]
        let cell = tableView.dequeueReusableCell(for: indexPath, cellIdentifier: vmItem.itemIdentifier)
        (cell as? ViewModelBindable)?.bind(to: vmItem)
        return cell
    }
}

//MARK: - UITableViewDataSource
extension PokemonDetailViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let title = self.viewModel.pokemonDetailViewModel.value[section].title
        if title.count > 0 {
            let headerView = HeaderView.init(frame: CGRect.init(x: 0, y: 0, width: self.tableView.frame.width, height: LayoutManager.headerHeight))
            headerView.configure(with: title)
            return headerView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let title = self.viewModel.pokemonDetailViewModel.value[section].title
        if title.count > 0 {
            return LayoutManager.headerHeight
        }
        return 0
    }
}
