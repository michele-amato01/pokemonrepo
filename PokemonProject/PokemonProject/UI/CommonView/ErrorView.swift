//
//  ErrorView.swift
//  Pokemon
//
//  Created by Michele Amato on 20/11/2020.
//

import UIKit

final class ErrorView: UIView {
    
    private enum Constants {
        static let font: UIFont = UIFont.systemFont(ofSize: 24.0, weight: .thin)
    }
   
    //MARK: - Properties
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Constants.font
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.gray
        return label
    }()
    
    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    //MARK: - Methods
    private func setup() {
        self.backgroundColor = UIColor.white
        addSubview(titleLabel)
        titleLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        titleLabel.text = "Error Data"
    }

}
