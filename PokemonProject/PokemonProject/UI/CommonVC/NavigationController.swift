//
//  NavigationController.swift
//  Pokemon
//
//  Created by Michele Amato on 15/11/2020.
//

import UIKit

final class NavigationController: UINavigationController, UINavigationControllerDelegate {
    
    //MARK: - Properties
    var rootViewController: UIViewController? {
        return viewControllers.first
    }
    
    //MARK: - Init
    override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        setup()
    }
    
    override init(navigationBarClass: AnyClass?, toolbarClass: AnyClass?) {
        super.init(navigationBarClass: navigationBarClass, toolbarClass: toolbarClass)
        setup()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    //MARK: - Methods
    private func setup() {
        delegate = self
        modalPresentationCapturesStatusBarAppearance = true
        self.navigationBar.titleTextAttributes = [.foregroundColor : UIColor.white,
                                                  .font : UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold)]
        self.navigationBar.isTranslucent = false
        self.navigationBar.shadowImage = UIImage()
    
        self.navigationBar.barTintColor = UIColor.red
        self.navigationBar.tintColor =  UIColor.white
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if viewController != rootViewController {
            navigationBar.backIndicatorImage = #imageLiteral(resourceName: "back")
            navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "back")
            navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
    }
    

}

extension UIViewController {
    
    var embeddedInNavigationController: UINavigationController {
        let navigationController = NavigationController(navigationBarClass: UINavigationBar.self, toolbarClass: nil)
        navigationController.viewControllers = [self]
        return navigationController
    }
}

