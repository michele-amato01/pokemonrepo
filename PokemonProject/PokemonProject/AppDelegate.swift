//
//  AppDelegate.swift
//  PokemonProject
//
//  Created by Michele Amato on 29/11/2020.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var pokemonCoordinator: PokemonCoordinator!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        let service: Service = Service.createService()
        self.pokemonCoordinator = PokemonCoordinator(service: service)
        let rootViewController = pokemonCoordinator.rootViewController
        let navigationController = rootViewController.embeddedInNavigationController
        navigationController.viewControllers = [rootViewController]
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()

        return true
    }
    
    
}

