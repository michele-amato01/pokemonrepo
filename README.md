## Pokemon

Pokemon is a simple app created that display a list of Pokémon with their detail.

It is base on MVVM Architecture, with an use of Observable object.
Observable it's an object initialized with the value to observe, bind function to binding data to views and listener called whenerever value is called.
A Coordinator Object was created to manage navigation between view controllers and functionalities. 

To fetch PokemonAPI, I created a simple Network Layer, using URLSession API, able to get data and managing custom errors, in a fast and testable mode, using a protocol-oriented programming protocol. On this struct there are an API structs, that represent each Pokemon API and a PokemonManager, an object using Network Layer, provide data for ViewModels.

To make app work offline, I thought two ways, Realm or CoreData usage.
CoreData requires a deep understaing of their API by the developer and requires to use for each business model a ManagedObjectContext ad and maintenance required is more harder than Realm.
As dependecy management tool I preferred CocoaPods system, because it's centralized, so it's very fast to create, add, update and delete the dependencies in the project. It is certainly the most complete as a number of framworks present.